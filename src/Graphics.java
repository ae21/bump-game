import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import model.AutoCar;
import model.Model;
import model.MyCar;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Graphics {
    private Model model;
    private GraphicsContext gc;

    private Image auto1 = new Image(new FileInputStream("src/images/greencar.png"));
    private Image auto2 = new Image(new FileInputStream("src/images/badcar.png"));
    private Image auto3 = new Image(new FileInputStream("src/images/player.png"));
    private Image gameover = new Image(new FileInputStream("src/images/ende.gif"));
    private Image win = new Image(new FileInputStream("src/images/juhu.gif"));
    private Image background = new Image(new FileInputStream("src/images/background.png"));


    public Graphics(Model model, GraphicsContext gc) throws FileNotFoundException {
        this.model = model;
        this.gc = gc;
    }

    public void draw() {
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);
        gc.drawImage(background, 0, 0, Model.WIDTH, Model.HEIGHT);

        for (AutoCar autoCar : this.model.getAutoCars()) {
            if (autoCar.isBadcar() == true) {
                gc.drawImage(auto2, autoCar.getX(), autoCar.getY(), autoCar.getW(), autoCar.getH());
            } else if (autoCar.isBadcar() == false && autoCar.isInvisible() == false) {
                gc.drawImage(auto1, autoCar.getX(), autoCar.getY(), autoCar.getW(), autoCar.getH());
            }
        }


        MyCar mycar = model.getMyCar();
        gc.drawImage(auto3, mycar.getX(), mycar.getY(), mycar.getW(), mycar.getH());


        if (model.isGameover() == true) {
            gc.drawImage(gameover, 0, 0, Model.WIDTH, Model.HEIGHT);
        }

        if (model.isWin() == true) {
            gc.drawImage(win, 0, 0, Model.WIDTH, Model.HEIGHT);
        }

    }
}
