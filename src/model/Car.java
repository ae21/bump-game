package model;

import java.util.Random;

public class Car {

    private int x;
    private int y;
    private float speed;
    private int angle;
    Random random = new Random();

    public Car(int x, int y, float speed, int angle) {
        this.x = x;
        this.y = y;
        this.speed = speed;
        this.angle = angle;
    }


    public void update(long elapsedTime) {

        this.angle = this.angle % 360;

        this.x = (int) Math.round(this.x + elapsedTime * this.speed * Math.sin(Math.toRadians(this.angle)));

        this.y = (int) Math.round(this.y + elapsedTime * this.speed * (Math.cos(Math.toRadians(this.angle))));

        if ((this.x > Model.WIDTH - getW() && this.angle < 180)
                || (this.x < 0 && this.angle > 180)) {
            this.angle = angle + 180;
        }

        if ((this.y > Model.HEIGHT - getH() && (this.angle < 90 || this.angle > 270))
                || (this.y < 0 && (this.angle < 270 && this.angle > 90))) {
            this.angle = angle + 180;
        }

    }


    public void setAngle(int angle) {
        this.angle = angle;
    }

    public int getAngle() {
        return angle;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() { return 40; }

    public int getW() {
        return 40;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getSpeed() {
        return speed;
    }
}
