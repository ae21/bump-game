package model;

public class MyCar extends Car {

    public MyCar(int x, int y, float speed, int angle) {
        super(x, y, speed, angle);
    }

    @Override
    public int getH() {
        return 50;
    }

    @Override
    public int getW() {
        return 50;
    }

}
