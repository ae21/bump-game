package model;

public class AutoCar extends Car{

    private boolean isBadcar;
    private boolean invisible = false;


    public AutoCar(int x, int y, float speed, int angle, boolean isBadcar) {
        super(x,y,speed, angle);
        this.isBadcar = isBadcar;
    }

    public void updateAuto(long elapsedTime) {

       super.update(elapsedTime);

       if (random.nextInt(80) == 0)
            changeDirection();
    }


    public void changeDirection() {
        super.setAngle(random.nextInt(360));
    }

    public boolean isInvisible() { return invisible; }

    public void setInvisible(boolean invisible) { this.invisible = invisible; }

    public boolean isBadcar() {
        return isBadcar;
    }
}
