package model;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Model {
    public static final int WIDTH = 1000;
    public static final int HEIGHT = 600;

    private List<AutoCar> autoCars = new LinkedList<>();
    private MyCar myCar;
    private boolean gameover;
    private boolean win;
    private int score = 0;

    public List<AutoCar> getAutoCars() {
        return autoCars;
    }

    public MyCar getMyCar() {
        return myCar;
    }

    public Model() {
        Random random = new Random();
        boolean setBad = false;

        for (int i = 0; i < 5; i++) {
            this.autoCars.add(
                    new AutoCar(
                            random.nextInt(800) + 200,
                            random.nextInt(400) + 200,
                            0.1f + random.nextFloat() * 0.2f,
                            random.nextInt(360),
                            setBad
                    )
            );
            setBad = !setBad;
        }

        this.myCar = new MyCar(10, 10, 0.2f, 0);
    }

    public void update(long elapsedTime) {
        for (AutoCar autoCar : autoCars) {
            autoCar.updateAuto(elapsedTime);
        }

        myCar.update(elapsedTime);

        for (AutoCar autoCar : autoCars) {
            int dx = Math.abs(myCar.getX() - autoCar.getX());
            int dy = Math.abs(myCar.getY() - autoCar.getY());
            int w = myCar.getW() / 2 + autoCar.getW() / 2;
            int h = myCar.getH() / 2 + autoCar.getH() / 2;

            if (dx <= w && dy <= h) {
                if (autoCar.isBadcar() == true) {
                    gameover = true;

                } else {
                    autoCar.setInvisible(true);
                    score++;
                }
            }

        }

        a:
        for (AutoCar autoCar : autoCars) {
            if (autoCar.isBadcar() == false && autoCar.isInvisible() == false) {
                win = false;
                break a;
            } else {
                win = true;
            }
        }
    }

    public boolean isWin() {
        return win;
    }

    public boolean isGameover() {
        return gameover;
    }

    public int getScore() {
        return score;
    }
}
