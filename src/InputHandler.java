import javafx.scene.input.KeyCode;
import model.Model;

public class InputHandler {
    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }


    public void onKeyPressed(KeyCode key) {

        if (key == KeyCode.UP) {
            model.getMyCar().setAngle(model.getMyCar().getAngle() + 180);
        } else if (key == KeyCode.DOWN) {
            model.getMyCar().setAngle(model.getMyCar().getAngle() + 180);
        } else if (key == KeyCode.LEFT) {
            model.getMyCar().setAngle(model.getMyCar().getAngle() + 90);
        } else if (key == KeyCode.RIGHT) {
            model.getMyCar().setAngle(model.getMyCar().getAngle() + 270);
        } else if (key == KeyCode.NUMPAD1 && model.getMyCar().getSpeed() < 0.3f) {
            model.getMyCar().setSpeed(model.getMyCar().getSpeed() + 0.01f);
        } else if (key == KeyCode.NUMPAD2 && model.getMyCar().getSpeed() > 0.1f) {
            model.getMyCar().setSpeed(model.getMyCar().getSpeed() - 0.01f);
        }

        model.getMyCar().setAngle(model.getMyCar().getAngle() % 360);

    }


}
